FROM ubuntu:20.04
WORKDIR /app
COPY . /app
RUN apt-get update -y
RUN apt-get install python3-dev -y
RUN apt-get install python3-venv -y
RUN apt install python3-pip -y
ENTRYPOINT ["python3"]
CMD ["cd/venv pip install Flask -y" "prueba.py"]
# para ejecutar el flask necesitamos hacer en la carpeta del archivo .py
# export FLASK_APP="nombre".py
# flask run